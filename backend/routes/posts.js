const express = require("express");
const Post = require('../models/post');
const checkAuth = require("../middleware/check-auth");
const multer = require("multer");

const router = express.Router();

const MIME_TYPE_MAP = {
    'image/png': 'png',
    'image/jpeg': 'jpg',
    'image/jpg': 'jpg'
};

// confuguration multer
const storage = multer.diskStorage({
    //For define the destination check the File Formate
    destination: (req, file, cb)=>{
        const isValid = MIME_TYPE_MAP[file.mimetype];
        let error = new Error("Invalid mime type");
        if(isValid){
            error = null;
        }
        cb(error, "backend/images"); //This path relativ to server.js
    },
    // Inform to multer File name
    filename: (req, file, cb) =>{
        const name = file.originalname.toLowerCase().split('').join('_');
        const ext = MIME_TYPE_MAP[file.mimetype];
        cb(null, name + '-' + Date.now() + '.' + ext);

    }
}
);

router.post("", checkAuth, multer({storage: storage}).single("image"), (req, res, next)=>{
    //constract a URL from our server
    const url = req.protocol + '://' + req.get("host");
    const post = new Post({
        title: req.body.title,
        content: req.body.content,
        imagePath: url + "/images/" + req.file.filename
    });
    post.save().then((createdPost)=>{
        res.status(201).json({
        message: "Post added successfully",
        post: {
            ...createdPost, // spread operator get all property form createdPost
            postId: createdPost._id, // overide some selected property

            //Alternatives
            // postId: createdPost._id,
            // content: createdPost.content,
            // imagePath: createdPost.imagePath,
        }
        });
    });
});

router.put("/:id", checkAuth, multer({storage: storage}).single("image"),(req, res, next)=>{
    let imagePath = req.body.imagePath;
    if(req.file){
        //constract a URL from our server
        const url = req.protocol + '://' + req.get("host");
        imagePath = url + "/images/" + req.file.filename
    }
    const post = new Post({
        _id: req.body.id,
        title: req.body.title,
        content: req.body.content,
        imagePath: imagePath

    });
    Post.updateOne({_id: req.params.id}, post).then(result =>{
        console.log(result);
        res.status(200).json({message: 'Update successful'});
    });
});

router.get("", (req, res, next) =>{
    // This allowed to us change Mongoose query
    const pageSize = +req.query.pageSize; //+convert to nr.
    const currentPage = +req.query.page;
    const postQuery = Post.find(); //Get all queries from DB
    let fetchedposts;
    if(pageSize && currentPage){
        postQuery
        .skip(pageSize * (currentPage - 1))
        .limit(pageSize);
    }
    postQuery // After postQuery go to then
    .then(documents =>{
        fetchedposts = documents;
        return Post.count(); // After return value move to then
    })
    .then(count=>{
        res.status(200).json({
            message:"Posts fetched succesfully!",
            posts: fetchedposts,
            maxCount: count,
        });
    });
});

router.get("/:id", (req, res, next)=> {
    Post.findById(req.params.id).then(post =>{
        if(post){
            res.status(200).json(post);

        }else{
            res.status(404).json({message: 'Post not found!'});
        }
    });
});

router.delete("/:id", checkAuth, (req, res, next)=> {
    Post.deleteOne({_id: req.params.id}).then(result => {
        console.log(result);
        res.status(200).json({message: "Post deleted!"});
    });
});

module.exports = router;

