import { Injectable } from "@angular/core";
import { Post } from './post.model'
import { Subject } from 'rxjs'
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({providedIn: "root"})
export class PostService{
    private posts: Post[] = [];
    private postsUpdated = new Subject<{posts: Post[], postCount: number}>();

    constructor(private httpClient: HttpClient, private router: Router){

    }

    getPosts(postPerPage: number, currentPage: number){
        //These are backticks. Which allows to dynamically add values into normal string
        const queryParams = `?pageSize=${postPerPage}&page=${currentPage}`;  //Orepare query params to get request
        this.httpClient.get<{message: string, posts: any, maxCount: number}>("http://localhost:3000/api/posts" + queryParams)
        .pipe(map((postData)=>{ //with help of pipe i can modify datas! 
            return { post: postData.posts.map(post=>{
                return{
                    title: post.title,
                    content: post.content,
                    id: post._id,
                    imagePath: post.imagePath
                };
            }), maxPosts: postData.maxCount};
        }))
        .subscribe((transformedPostData)=>{
            this.posts = transformedPostData.post;
            this.postsUpdated.next({posts: [...this.posts], postCount: transformedPostData.maxPosts});
        });
    }

    getPostUpdateListener(){
        return this.postsUpdated.asObservable();
    }

    addPost(title: string, content: string, image: File){
        const postData = new FormData(); //Combined text value and File
        postData.append("title", title);
        postData.append("content", content);
        postData.append("image", image, title);
        this.httpClient
        .post<{message: string, post: Post}>(
            "http://localhost:3000/api/posts", 
            postData
        )
        .subscribe((responseData =>{
            this.router.navigate(["/"]);
        }));
    }

    updatePost(id: string, title: string, content: string, image: File | string){
        let postData: Post | FormData;

        // check image type object(File) or string --> object need FormData to send request
        if(typeof(image) === 'object'){
            postData = new FormData(); //Combined text value and File
            postData.append("id", id);
            postData.append("title", title);
            postData.append("content", content);
            postData.append("image", image, title);
        }else{
            postData = {id: id, title: title, content: content, imagePath: image};

        }
        this.httpClient.put("http://localhost:3000/api/posts/" + id, postData)
        .subscribe(response =>{
            this.router.navigate(["/"]);
        });
    }

    deletePost(postId: string){
        return this.httpClient.delete("http://localhost:3000/api/posts/" + postId)
    }

    getPost(id: string){
        return this.httpClient.get<{_id: string, title: string, content: string, imagePath: string}>("http://localhost:3000/api/posts/" + id);
    }
}