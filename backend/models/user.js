const mongoose = require('mongoose');
const uniqValidator = require('mongoose-unique-validator'); // You have to use like plugin

//Blue print
const userSchema = mongoose.Schema({
  // unique doesn't work like validator and allow internal optimization
  email: {type: String, require: true, unique: true},
  password: {type: String, require: true}
});

//Extra function which mongoose will run on the schema
//Check the Data bevor save it
//Email adress will check becaue i allowed with unique: true
userSchema.plugin(uniqValidator);

//mongoose need model
module.exports = mongoose.model('User', userSchema);
