const mongoose = require('mongoose');

//blue print
const postSchema = mongoose.Schema({
    title: {type: String, required: true},
    content: {type: String, required: true},
    imagePath: {type: String, require: true}
});

// mongoose need model
module.exports = mongoose.model('Post', postSchema);
