const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const postsRoutes = require("./routes/posts");
const usersRoutes = require("./routes/user");

const app = express();

// mongoose.connect("mongodb+srv://Sinan:UzQ8GkvWVsz74IvH@cluster0.xamba.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
// .then(()=>{
//     console.log("Connected to database!");
// })
// .catch(()=>{
//     console.log("Connection Failed!");
// });

mongoose.connect("mongodb://localhost:27017/myFirstDatabase?retryWrites=true&w=majority")
.then(()=>{
    console.log("Connected to database!");
})
.catch(()=>{
    console.log("Connection Failed!");
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false})); // This is just example! you can parse different bodys
app.use("/images", express.static(path.join("backend/images"))); //static means any request targeting "/images will be allow continue

app.use((req, res, next)=>{
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept",
        "Authorization"
    );
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, PATCH, PUT, DELETE, OPTIONS"
    );
    next();
});

app.use("/api/posts", postsRoutes);
app.use("/api/user",usersRoutes);
module.exports = app;
