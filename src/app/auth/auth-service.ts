import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { authData } from "./auth-data.model";

@Injectable({providedIn: "root"})
export class AuthService{
  private token: string;

  constructor(private httpClient: HttpClient){}

  // Get the token
  getToken(){
    return this.token;
  }

  //To create a new user send a post req. with authData
  createUser(email: string, password: string){
    const authData: authData = {email: email, password: password};
    this.httpClient.post("http://localhost:3000/api/user/signup", authData)
    .subscribe(response =>{
      console.log(response);
    });
  }

  login(email: string, password: string){
    const authData: authData = {email: email, password: password};
    //post request with authData and return token
    this.httpClient.post<{token: string}>("http://localhost:3000/api/user/login", authData)
    .subscribe(response =>{
      const token = response.token;
      this.token = token;
    });
  }

}
