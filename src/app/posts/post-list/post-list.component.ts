import { Component, OnDestroy, OnInit } from "@angular/core";
import { Post } from '../post.model'
import { PostService } from '../post.service';
import { Subscription } from "rxjs"; 
import { PageEvent } from "@angular/material/paginator";

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit, OnDestroy{

  posts: any[]=[];
  private postSub : Subscription;
  isLoading = false;
  totalPosts = 0;
  postPerPage = 2;
  currentPage = 1;
  pageSizeOptions = [1, 2, 5, 10];  

  ngOnInit() : void {
    this.isLoading = true;
    this.postService.getPosts(this.postPerPage, this.currentPage);
    this.postService.getPostUpdateListener().subscribe((postData: {posts: Post[], postCount: number}) => {
      this.isLoading = false;
      this.totalPosts = postData.postCount;
      console.log(postData.postCount);
      this.posts = postData.posts;

    });
  }
  ngOnDestroy(): void {
    if(this.postSub){
      this.postSub.unsubscribe();
    }
  }

  onDelete(postId: string){
    this.postService.deletePost(postId).subscribe(() =>{
      this.postService.getPosts(this.postPerPage, this.currentPage);
    });

  }

  constructor(public postService: PostService){

  }

  onChangedPage(pageData: PageEvent){
    this.isLoading = true;
    this.postPerPage = pageData.pageSize;
    this.currentPage = pageData.pageIndex + 1;
    this.postService.getPosts(this.postPerPage, this.currentPage);

  }

}
