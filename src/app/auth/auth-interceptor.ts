import { HttpInterceptor, HttpRequest, HttpHandler } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthService } from "./auth-service";


@Injectable()
export class AuthInterceptor implements HttpInterceptor{

  constructor(private authService: AuthService){}

  intercept(req: HttpRequest<any>, next: HttpHandler){
     //Get the AuthToken
    const authToken = this.authService.getToken();
    //Clone the Request and set header with Token
    const authRequest = req.clone({
      headers: req.headers.set("Authorization", "Bearer " + authToken)
    });

    return next.handle(authRequest);
  }

}

