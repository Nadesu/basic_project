const express = require("express");
const User = require('../models/user');
const bcryptjs = require("bcryptjs");
const jsonwebtoken = require("jsonwebtoken");
const user = require("../models/user");
const router = express.Router();


router.post("/signup", (req, res, next)=>{
  //Create a hash password
  bcryptjs.hash(req.body.password, 10)
  //pass the PW to new user
  .then(hash => {
    const user = new User({
      email: req.body.email,
      password: hash
    });
    //save the user in DB
    user.save()
    .then(result =>{
      res.status(201).json({
        message: 'User created!',
        result: result
      });
    })
    .catch(err =>{
      res.status(500).json({
        error: err
      });
    });
  });
});

router.post("/login", (req, res, next)=>{
  //Exit user with this email
  User.findOne({email: req.body.email})
  .then(user =>{
    //User doesn't exit pass message
    if(!user){
      return res.status(401).json({
        message: 'auth failed!'
      });
    }
    return bcryptjs.compare(req.body.password, user.password); // compare pw this hash
  })
  .then(result =>{
    // Check the result
    if(!result){
      return res.status(401).json({
        message: 'auth failed!'
      });
    }
    //create a json web token
    const token = jsonwebtoken.sign(
      {email: user.email, userId: user._id},
      "secret_this_should_be_longer",
      {expiresIn: "1h"}
      );
      res.status(200).json({
        //Response the token
        token: token
      });
  })
  .catch(err =>{
    return res.status(401).json({
      message: 'auth failed!'
    });
  });
});



module.exports = router;
