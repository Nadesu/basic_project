import { AbstractControl } from "@angular/forms";
import { Observable, Observer, of } from "rxjs";

//Asynchronus validator return Observable or Promise
//Observable wrapped Error
export const mimeType = (control: AbstractControl): Promise<{[key: string]: any}> | Observable<{[key: string]: any}> =>{

    // If it string simply return it valid
    if(typeof(control.value) === 'string'){
        return of(null);
    }

    const file = control.value as File;
    const fileReader = new FileReader();

    //create a fileReader Observable
    const fileReaderObservable = new Observable((observer: Observer<{[key: string]: any}>) =>{
        fileReader.addEventListener("loadend", ()=>{
            const arr = new Uint8Array(fileReader.result as ArrayBuffer).subarray(0, 4); // unsigned 8bit array 
            
            let header ="";
            let isValid = false;
            for(let i = 0; i < arr.length; i++){
                header += arr[i].toString(16); // Hexadecimal value 
            }
            //Here i check for certain patterns and these are stand for certain file type 
            //Mime Type
            switch(header){
                case "89504e47":
                     isValid = true;
                    break;
                case "ffd8ffe0":
                case "ffd8ffe1":
                case "ffd8ffe2":
                case "ffd8ffe3":
                case "ffd8ffe8":
                    isValid = true;
                    break;
                default:
                    isValid = false; // Or you can use the blob.type as fallback
                    break;
            }
            if(isValid){
                observer.next(null); // null means it's valid
            }else{
                observer.next({ invalidMimeType: true}); // here you can add owen error 
            }
            observer.complete();
        });
        fileReader.readAsArrayBuffer(file);
    });
    return fileReaderObservable;
};